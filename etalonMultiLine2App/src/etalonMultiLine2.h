#include <iocsh.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExport.h>
#include <string.h>
#include <epicsExit.h>

#include <signal.h>
#include "spdlog/spdlog.h"

#include <asynPortDriver.h>
#include <asynOctetSyncIO.h>

#define EPICS_THREAD_CAN_JOIN

#define MAX_NUM_OF_CHANNELS 40
#define MAX_QUERY_SIZE 1000
#define MAX_REPLY_SIZE 1000
#define TIMEOUT          20

#define CHANNEL_SEPARATOR               "_"
#define VALUE_WITHIN_CHANNEL_SEPARATOR  ","

#define ANALYSIS_ERROR_BIT_POSITION             0
#define BEAM_BREAK_BIT_POSITION                 1
#define TEMPERATURE_ERROR_BIT_POSITION          2
#define MOTION_TOLERANCE_ERROR_BIT_POSITION     3
#define INTENSITY_ERROR_BIT_POSITION            4
#define CONNECTION_ERROR_USB_BIT_POSITION       5
#define TCP_SERVER_ERROR_BIT_POSITION           6
#define ERROR_SETTING_LASER_SPEED_BIT_POSITION  7
#define LASER_TEMPERATURE_ERROR_BIT_POSITION    8
#define DAQ_ERROR_DURING_DATA_ACQ_BIT_POSITION  9
#define TCP_SERVER_ERROR_FINAL_BIT_POSITION     10

class EtalonMultiline2 : public asynPortDriver
{

public:
    EtalonMultiline2(const char *portName, const char *octetPortName, bool enableDebug);
    ~EtalonMultiline2();
    asynStatus serverQuery(const char * query, char * reply, asynStatus status);
    void devicePolling();
    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus readInt32Array(asynUser *pasynUser, epicsInt32 *value, size_t nElements, size_t *nIn);
    asynStatus readFloat64Array(asynUser *pasynUser, epicsFloat64 *value, size_t nElements, size_t *nIn);

protected:
    asynStatus performMeasurement(asynStatus status);
    asynStatus getAlignment(asynStatus status);
    asynStatus createPollingThread(const char *octetPortName, asynStatus status);

    // Polling parameters
    int FESOption_;
    int IntensHundPerc_;
    int IsConnected_;
    int IsGrouped_;
    int LaserReady_;
    int NumChannels_;
    int ThresholdIntensityMax_;
    int ThresholdIntensityMin_;
    int RedPilotLaser_;
    int FrontEndSplitter_;
    int SingleMeasurement_;
    int PollingPeriod_;
    int PollingCounter_;
    int PollingEnable_;
    int MeasState_;
    int AlignmentStart_;
    int CurrentTask_;

    int Gains_;
    int SelectedChannels_;
    int MeasEnableChannel_;
    int MeasChannelPreshot_;
    
    int EnvironmentDataHum_;
    int EnvironmentDataPress_;
    int EnvironmentDataTemp_;
    int LaserUncertaintyFix_;
    int LaserUncertaintyLDep_;
    
    int AlignDataMax_;
    int AlignDataMin_;

    int GlobalMeasCounter_;

    int MeasCounter_;
    int MeasuredLength_;
    int MeasuredImin_;
    int MeasuredImax_;
    int MeasuredTemp_;
    int MeasuredPress_;
    int MeasuredHum_;

    int LastValidMeasCounter_;
    int LastValidMeasuredLength_;
    int LastValidMeasuredImin_;
    int LastValidMeasuredImax_;
    int LastValidMeasuredTemp_;
    int LastValidMeasuredPress_;
    int LastValidMeasuredHum_;

    int ErrorMask_;
    int MeasuredAnalysisError_;
    int MeasuredBeamInterruption_;
    int MeasuredTemperatureError_;
    int MeasuredMovementToleranceError_;
    int MeasuredIntensityError_;
    int MeasuredUsbConnectionError_;
    int MeasuredTcpServerError_;
    int MeasuredLaserSpeedError_;
    int MeasuredLaserTemperatureError_;
    int MeasuredDaqError_;
    int TcpFinalServerError_;

private:
    asynUser  *pasynUserOctet_;

    int threadRunning_ = 0;

    epicsEventId startMeasEvent_;
    epicsEventId startAlignEvent_;

    epicsThreadId pollingThreadId; 
    epicsThreadId singleMeasThreadId; 
    
    epicsInt32 numSelectedChannels;

    epicsInt32 channelList_[MAX_NUM_OF_CHANNELS];
    epicsInt32 gains_[MAX_NUM_OF_CHANNELS];
    epicsInt32 measChEna_[MAX_NUM_OF_CHANNELS];
    epicsFloat64 measPreshot_[MAX_NUM_OF_CHANNELS];
    epicsInt32 errorMask_[MAX_NUM_OF_CHANNELS];
    epicsInt32 measCounter_[MAX_NUM_OF_CHANNELS];
    epicsInt32 lastValidMeasCounter_[MAX_NUM_OF_CHANNELS];

    epicsInt32 pollingEnable_;
    epicsInt32 initialPolling_;
    epicsInt64 measureCurrentID_;

    enum MeasurementState
    {
        StateIdle,
        StateRequested,
        StateMeasuring,
        StateProcessing,
        StateError
    };

    enum TaskState
    {
        TaskIdle,
        TaskPolling,
        TaskMeasuring,
        TaskAligning
    };
};
