#include "etalonMultiLine2.h"

static const char *driverName = "etalonMultiLine2";

EtalonMultiline2 * etalonPtr;

static void c_devicepolling(void *arg)
{
  EtalonMultiline2 *p = (EtalonMultiline2 *)arg;
  p->devicePolling();
}

// static void c_performmeasurement(void *arg)
// {
//     EtalonMultiline2 *p = (EtalonMultiline2 *)arg;
//     p->performMeasurement();
// }

void Handle(int s)
{
  spdlog::info("Keyboard interrupt received, shutting IOC down.");
  epicsExit(0);
 }

/* Function called when 'exit' is issued on IOC shell*/
static void etalonMultilineShutdown(void* arg) {
    delete(etalonPtr);
    spdlog::info("Stopping driver {}", driverName);
}

EtalonMultiline2::EtalonMultiline2(const char *portName, 
                                   const char *octetPortName,
                                   bool enableDebug) 
                                    : asynPortDriver(portName, 1,
                                                     asynOctetMask | asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynInt32ArrayMask | asynDrvUserMask ,          // Interfaces that we implement
                                                     asynOctetMask | asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynInt32ArrayMask,       // Interfaces that do callbacks
                                                     ASYN_MULTIDEVICE | ASYN_CANBLOCK, 1, /* ASYN_CANBLOCK=1, ASYN_MULTIDEVICE=1, autoConnect=1 */
                                                     0, 0)  /* Default priority and stack size */
{
    createParam("FES_OPTION",                   asynParamInt32,         &FESOption_);
    createParam("INTENSITY_HUNDRED_PERCENT",    asynParamInt32,         &IntensHundPerc_);
    createParam("IS_CONNECTED",                 asynParamInt32,         &IsConnected_);
    createParam("IS_GROUPED",                   asynParamInt32,         &IsGrouped_);
    createParam("LASER_READY",                  asynParamInt32,         &LaserReady_);
    createParam("NUM_CHANNELS",                 asynParamInt32,         &NumChannels_);
    createParam("THRESHOLD_INTENSITY_MAX",      asynParamInt32,         &ThresholdIntensityMax_);
    createParam("THRESHOLD_INTENSITY_MIN",      asynParamInt32,         &ThresholdIntensityMin_);
    createParam("RED_PILOT_LASER",              asynParamInt32,         &RedPilotLaser_);
    createParam("FRONT_END_SPLITTER",           asynParamInt32,         &FrontEndSplitter_);
    createParam("SINGLE_MEASUREMENT",           asynParamInt32,         &SingleMeasurement_);
    createParam("POLLING_PERIOD",               asynParamInt32,         &PollingPeriod_);
    createParam("POLLING_COUNTER",              asynParamInt32,         &PollingCounter_);
    createParam("POLLING_ENABLE",               asynParamInt32,         &PollingEnable_);
    createParam("MEAS_STATE",                   asynParamInt32,         &MeasState_);
    createParam("ALIGNMENT_START",              asynParamInt32,         &AlignmentStart_);
    createParam("CURRENT_TASK",                 asynParamInt32,         &CurrentTask_);
    
    createParam("GAINS",                        asynParamInt32Array,    &Gains_);
    createParam("SELECTED_CHANNELS",            asynParamInt32Array,    &SelectedChannels_);
    createParam("MEAS_ENABLE_CHANNEL",          asynParamInt32Array,    &MeasEnableChannel_);
    createParam("MEAS_CHANNEL_PRESHOT",         asynParamFloat64Array,  &MeasChannelPreshot_);
    
    createParam("ENVIRONMENT_DATA_HUMIDITY",    asynParamFloat64,       &EnvironmentDataHum_);
    createParam("ENVIRONMENT_DATA_PRESSURE",    asynParamFloat64,       &EnvironmentDataPress_);
    createParam("ENVIRONMENT_DATA_TEMPERATURE", asynParamFloat64,       &EnvironmentDataTemp_);
    createParam("LASER_UNCERTAINTY_FIX",        asynParamFloat64,       &LaserUncertaintyFix_);
    createParam("LASER_UNCERTAINTY_LDEP",       asynParamFloat64,       &LaserUncertaintyLDep_);
    
    createParam("ALIGN_DATA_MAX",               asynParamFloat64Array,  &AlignDataMax_);
    createParam("ALIGN_DATA_MIN",               asynParamFloat64Array,  &AlignDataMin_);

    createParam("GLOBAL_MEAS_COUNTER",          asynParamInt32,        &GlobalMeasCounter_);

    createParam("MEAS_COUNTER",                 asynParamInt32Array,   &MeasCounter_);
    createParam("MEASURED_LENGTH",              asynParamFloat64Array, &MeasuredLength_);
    createParam("MEASURED_IMIN",                asynParamInt32Array,   &MeasuredImin_);
    createParam("MEASURED_IMAX",                asynParamInt32Array,   &MeasuredImax_);
    createParam("MEASURED_TEMP",                asynParamFloat64Array, &MeasuredTemp_);
    createParam("MEASURED_PRESS",               asynParamFloat64Array, &MeasuredPress_);
    createParam("MEASURED_HUM",                 asynParamFloat64Array, &MeasuredHum_);

    createParam("LAST_VALID_MEAS_COUNTER",      asynParamInt32Array,   &LastValidMeasCounter_);
    createParam("LAST_VALID_MEASURED_LENGTH",   asynParamFloat64Array, &LastValidMeasuredLength_);
    createParam("LAST_VALID_MEASURED_IMIN",     asynParamInt32Array,   &LastValidMeasuredImin_);
    createParam("LAST_VALID_MEASURED_IMAX",     asynParamInt32Array,   &LastValidMeasuredImax_);
    createParam("LAST_VALID_MEASURED_TEMP",     asynParamFloat64Array, &LastValidMeasuredTemp_);
    createParam("LAST_VALID_MEASURED_PRESS",    asynParamFloat64Array, &LastValidMeasuredPress_);
    createParam("LAST_VALID_MEASURED_HUM",      asynParamFloat64Array, &LastValidMeasuredHum_);
    
    createParam("ERROR_MASK",                        asynParamInt32Array,  &ErrorMask_);
    createParam("MEASURED_ANALYSIS_ERROR",           asynParamInt32Array,  &MeasuredAnalysisError_);
    createParam("MEASURED_BEAM_INTERRUPTION",        asynParamInt32Array,  &MeasuredBeamInterruption_);
    createParam("MEASURED_TEMPERATURE_ERROR",        asynParamInt32Array,  &MeasuredTemperatureError_);
    createParam("MEASURED_MOVEMENT_TOLERANCE_ERROR", asynParamInt32Array,  &MeasuredMovementToleranceError_);
    createParam("MEASURED_INTENSITY_ERROR",          asynParamInt32Array,  &MeasuredIntensityError_);
    createParam("MEASURED_USB_CONNECTION_ERROR",     asynParamInt32Array,  &MeasuredUsbConnectionError_);
    createParam("MEASURED_TCP_SERVER_ERROR",         asynParamInt32Array,  &MeasuredTcpServerError_);
    createParam("MEASURED_LASER_SPEED_ERROR",        asynParamInt32Array,  &MeasuredLaserSpeedError_);
    createParam("MEASURED_LASER_TEMPERATURE_ERROR",  asynParamInt32Array,  &MeasuredLaserTemperatureError_);
    createParam("MEASURED_DAQ_ERROR",                asynParamInt32Array,  &MeasuredDaqError_);
    createParam("TCP_FINAL_SERVER_ERROR",            asynParamInt32,  &TcpFinalServerError_);
    
    /* Enable debug level */
    if(enableDebug)
        spdlog::set_level(spdlog::level::debug);

    /* Define keyboard interrupt behaviour */
    signal(SIGINT, Handle);

    /* Define what to do when user exit EPICS shell */
    epicsAtExit(etalonMultilineShutdown, (void*)this);

    /* Create and configure events */
    startMeasEvent_ = epicsEventCreate(epicsEventEmpty);
    if (!startMeasEvent_)
    {
        spdlog::error("Failure on event creation");
    }

    startAlignEvent_ = epicsEventCreate(epicsEventEmpty);
    if (!startAlignEvent_)
    {
        spdlog::error("Failure on event creation");
    }

    asynStatus status = asynSuccess;
    
    /**
     *  Initialize polling
     *      pollingEnable_: Don't start it automatically
     *      initialPolling_: Poll once to initialize few variables
     */
    initialPolling_ = 1;
    if(!status)
        status = setIntegerParam(PollingCounter_, 0);

    /* Initialize measurement state */
    if(!status)
       status = setIntegerParam(MeasState_, StateIdle);

    /* Initialize task reporter */
    if(!status)
        status = setIntegerParam(CurrentTask_, TaskIdle);

    /* Initialize measure counter array */
    for(int i = 0; i < MAX_NUM_OF_CHANNELS; i++)
        measCounter_[i] = 0;

    if(!status)
        status = doCallbacksInt32Array(measCounter_, MAX_NUM_OF_CHANNELS, MeasCounter_, 0);
    
    /* Start main operations thread */
    threadRunning_ = 1;
    status = createPollingThread(octetPortName, status);
    if(status)
        spdlog::error("Initialization error. asynStatus = {}", status);
}

EtalonMultiline2::~EtalonMultiline2()
{
    pollingEnable_ = 0;
    threadRunning_ = 0;
    spdlog::info("Please wait");
    epicsThreadMustJoin(pollingThreadId);
    pasynOctetSyncIO->disconnect(pasynUserOctet_);
}

asynStatus EtalonMultiline2::createPollingThread(const char *octetPortName, asynStatus status)
{
    const char *inputEos = "\r\n";
    const char *outputEos = "\r\n";
    
    if(!status)
        status = pasynOctetSyncIO->connect(octetPortName, 0, &pasynUserOctet_, NULL);
    if(!status)
        status = pasynOctetSyncIO->setInputEos(pasynUserOctet_, inputEos, strlen(inputEos));
    if(!status)
        status = pasynOctetSyncIO->setOutputEos(pasynUserOctet_, outputEos, strlen(outputEos));
    
    /* Create and configure thread*/
    epicsThreadOpts opts = EPICS_THREAD_OPTS_INIT;

    opts.stackSize = epicsThreadGetStackSize(epicsThreadStackMedium);
    opts.priority = epicsThreadPriorityMedium;
    opts.joinable = 1;

    pollingThreadId = epicsThreadCreateOpt("DevicePolling",
                                            c_devicepolling,
                                            this,
                                            &opts);
    return status;
}

asynStatus EtalonMultiline2::serverQuery(const char * query,
                                         char * reply,
                                         asynStatus status = asynSuccess)
{
    const char *functionName = "serverQuery";

    size_t nRead = 15;
    size_t nWrite = 13;

    int eomReason;

    if(!status && threadRunning_)
        status = pasynOctetSyncIO->writeRead(pasynUserOctet_,
                                             query, strlen(query),
                                             reply, MAX_REPLY_SIZE,
                                             TIMEOUT,
                                             &nWrite, &nRead, &eomReason);
    spdlog::debug("query: {}", query);
    spdlog::debug("reply: {}", reply);

    if (status)
    {
        spdlog::error("{}::{} query: {} asynStatus: {}", driverName, functionName, query, status);
        threadRunning_ = 0;
    }
    return status;
}

void EtalonMultiline2::devicePolling()
{
    asynStatus status = asynSuccess;
    epicsEventStatus statusEvent;

    char reply[MAX_QUERY_SIZE];
    char query[MAX_REPLY_SIZE];
    int pollingCounterLocal = 0;
    
    while(threadRunning_)
    {
        /* Polling section*/
        if(initialPolling_)
            pollingEnable_ = 1;

        /* Integers */
        {
            int value1;
            int value2;

            if(pollingEnable_)
            {
                status = setIntegerParam(CurrentTask_, TaskPolling);
                if(!status)
                    status = callParamCallbacks();
            }
            
            if(pollingEnable_)
            {
                status = serverQuery("isconnected", reply, status);
                sscanf(reply, "isconnected_%d", &value1);
                if(!status)
                    status = setIntegerParam(IsConnected_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("laserready", reply, status);
                sscanf(reply, "laserready_%d", &value1);
                if(!status)
                    status = setIntegerParam(LaserReady_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("isgrouped", reply, status);
                sscanf(reply, "isgrouped_%d", &value1);
                if(!status)
                    status = setIntegerParam(IsGrouped_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("numchannels", reply, status);
                sscanf(reply, "numchannels_%d", &value1);
                if(!status)
                    status = setIntegerParam(NumChannels_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("intensityhundredpercent", reply, status);
                sscanf(reply, "intensityhundredpercent_%d", &value1);
                if(!status)
                    status = setIntegerParam(IntensHundPerc_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("getfesoption", reply, status);
                sscanf(reply, "getfesoption_%d", &value1);
                if(!status)
                    status = setIntegerParam(FESOption_, value1);
            }

            if(pollingEnable_)
            {
                status = serverQuery("thresholdintensity", reply, status);
                sscanf(reply, "thresholdintensity_%d,%d", &value1, &value2);
                if(!status)
                {
                    status = setIntegerParam(ThresholdIntensityMin_, value1);
                    status = setIntegerParam(ThresholdIntensityMax_, value2);
                }
            }

            if(pollingEnable_)
            {
                status = serverQuery("selectedchannels", reply, status);
            
                if(!status)
                {
                    char * token;
                    int index = 0;          
            
                    token = strtok(reply, "_,");
                    token = strtok(NULL, "_,");

                    while(token != NULL)
                    {
                        channelList_[index] = atoi(token);
                        token = strtok(NULL, "_,");
                        index++;
                    }

                    numSelectedChannels = index;

                    status = doCallbacksInt32Array(channelList_, MAX_NUM_OF_CHANNELS, SelectedChannels_, 0);
                }
            }

            if(pollingEnable_)
            {
                if(!status)
                {
                    strcpy(query, "gains");

                    char strChannel[4];

                    for(int i = 0; i < numSelectedChannels; i++)
                    {
                        sprintf(strChannel, ",%d", channelList_[i]);
                        strcat(query, strChannel);
                    }
                }

                status = serverQuery(query, reply, status);

                if(!status)
                {
                    char * token;
                    int index = 0;

                    token = strtok(reply, "_");
                    token = strtok(NULL, "_");

                    epicsInt32 channel;
                    while(token != NULL)
                    {
                        sscanf(token, "%d,%d", &channel, gains_ + index);
                        token = strtok(NULL, "_");
                        index++;
                    }
                    status = doCallbacksInt32Array(gains_, MAX_NUM_OF_CHANNELS, Gains_, 0);
                }
            }
        }
        
        /* Doubles */
        {
            double value1;
            double value2;
            double value3;

            if(pollingEnable_)
            {
                status = serverQuery("uncertaintieslaser", reply, status);
                sscanf(reply, "uncertaintieslaser_%lf,%lf", &value1, &value2);
                if(!status)
                    status = setDoubleParam(LaserUncertaintyFix_, value1);
                if(!status)
                    status = setDoubleParam(LaserUncertaintyLDep_, value2);
            }

            if(pollingEnable_)
            {
                status = serverQuery("environmentdata", reply, status);
                sscanf(reply, "environmentdata_%lf,%lf,%lf", &value1, &value2, &value3);
                if(!status)
                    status = setDoubleParam(EnvironmentDataTemp_, value1);
                if(!status)
                    status = setDoubleParam(EnvironmentDataPress_, value2);
                if(!status)
                    status = setDoubleParam(EnvironmentDataHum_, value3);
            }

            if(pollingEnable_)
                status = getAlignment(status);
        }

        /* Finalize a poll */
        if(pollingEnable_)
        {
            epicsInt32 pollingPeriodTemp;
            status = getIntegerParam(PollingPeriod_, &pollingPeriodTemp);
            epicsThreadSleep(pollingPeriodTemp);
            
            pollingCounterLocal++;
            status = setIntegerParam(PollingCounter_, pollingCounterLocal);

            if(!status)
                status = callParamCallbacks();
        }
        else
        {
            if(!status)
                status = setIntegerParam(CurrentTask_, TaskIdle);
            if(!status)
                status = callParamCallbacks(); 
        }

        if(initialPolling_)
        {
            pollingEnable_ = 0;
            initialPolling_ = 0;            
        }

        /* Measurement section */
        statusEvent = epicsEventTryWait(startMeasEvent_);
        if(statusEvent ==  epicsEventOK)
        {
            if(!status)
                status = setIntegerParam(CurrentTask_, TaskMeasuring);
            if(!status)
                status = callParamCallbacks();
            status = performMeasurement(status);
        }

        /* Alignment section */
        statusEvent = epicsEventTryWait(startAlignEvent_);
        if(statusEvent ==  epicsEventOK)
        {
            if(!status)
                status = serverQuery("startalignment", reply);

            if(strcmp(reply, "startalignment_0") != 0)
                spdlog::error("Reply \"{}\" not expected", reply);


            if(!status)
                status = setIntegerParam(AlignmentStart_, 1);
            
            if(!status)
                status = setIntegerParam(CurrentTask_, TaskAligning);
            
            if(!status)
                status = callParamCallbacks();
            
            int localAlignmentEnable = 1;
            
            while(threadRunning_ && localAlignmentEnable)
            {
                status = getAlignment(status);
                status = getIntegerParam(AlignmentStart_, &localAlignmentEnable);
            }

            if(!status)
                status = serverQuery("stopalignment", reply);

            if(strcmp(reply, "stopalignment_0") != 0)
                spdlog::error("Reply \"{}\" not expected", reply);
        }

        if(status)
        {
            threadRunning_ = 0;
            spdlog::error("Main operations thread failure. asynStatus: {}", status);
        }
    }

    char threadName[14];
    epicsThreadGetName (pollingThreadId, threadName, 14*sizeof(char));
    spdlog::debug("Exited thread \"{}\".", threadName);      
}

asynStatus EtalonMultiline2::performMeasurement(asynStatus status)
{
    char query[MAX_QUERY_SIZE];
    char reply[MAX_REPLY_SIZE];
    short int enabledChannelsCounter = 0;
    if(!status)
    {
        strcpy(query, "measurelength");

        char tempBuffer[10];
        for(int i = 0; i < numSelectedChannels; i++)
        {
            if(measChEna_[i])
            {
                strcat(query,",");
                sprintf(tempBuffer, "%d", channelList_[i]);
                strcat(query, tempBuffer);
                strcat(query,",");
                sprintf(tempBuffer, "%lf", measPreshot_[i]);
                strcat(query, tempBuffer);
                enabledChannelsCounter++;
            }
        }
    }
    if(enabledChannelsCounter)
    {
        if(!status)
            status = setIntegerParam(MeasState_, StateMeasuring);
        if(!status)
            status = callParamCallbacks();

        status = serverQuery(query, reply, status);

        if(strcmp(reply, "ERROR") == 0)
        {
            spdlog::error("Server replied error for the following request: {}", query);
        
            if(!status)
                status = setIntegerParam(MeasState_, StateError);
        
            if(!status)
                status = callParamCallbacks();
        }
        else if(strcmp(reply, "measurementfinished") == 0)
        {
            if(!status)
                status = setIntegerParam(MeasState_, StateProcessing);
            
            if(!status)
                status = callParamCallbacks();
            
            if(!status)
            {
                size_t nbytesTransfered;
                int eomReason;
                status = pasynOctetSyncIO->read(pasynUserOctet_, 
                                                reply, 1000*sizeof(char),
                                                TIMEOUT,
                                                &nbytesTransfered, &eomReason);
                spdlog::debug("reply: {}", reply);
            }

            if(strcmp(reply, "ERROR") == 0)
            {
                spdlog::error("Error after processing. Query: {} Reply: {}. Did you set the preshot != 0?", query, reply);
                
                if(!status)
                    status = setIntegerParam(MeasState_, StateError);
                
                if(!status)
                    status = callParamCallbacks();
            }
            else
            {
                double measuredLength[MAX_NUM_OF_CHANNELS];
                int minimumIntensityRaw[MAX_NUM_OF_CHANNELS];
                int maximumIntensityRaw[MAX_NUM_OF_CHANNELS];
                double temperature[MAX_NUM_OF_CHANNELS];
                double airPressure[MAX_NUM_OF_CHANNELS];
                double humidity[MAX_NUM_OF_CHANNELS];
                int analysisError[MAX_NUM_OF_CHANNELS];
                int beamBreak[MAX_NUM_OF_CHANNELS];
                int temperatureError[MAX_NUM_OF_CHANNELS];
                int motionToleranceError[MAX_NUM_OF_CHANNELS];
                int intensityError[MAX_NUM_OF_CHANNELS];
                int connectionErrorUSB[MAX_NUM_OF_CHANNELS];
                int tcpServerError[MAX_NUM_OF_CHANNELS];
                int errorSettingLaserSpeed[MAX_NUM_OF_CHANNELS];
                int laserTemperatureError[MAX_NUM_OF_CHANNELS];
                int daqErrorDuringDataAcq[MAX_NUM_OF_CHANNELS];
                int tcpServerErrorFinal;
                int errorResult[MAX_NUM_OF_CHANNELS];
                if(!status)
                {
                    char * token;

                    token = strtok(reply, "_");
                    token = strtok(NULL, "_");

                    int channelNumber;

                    // Update valid measure ID
                    int globalMeasCounter;
                    if(!status)
                        status = getIntegerParam(GlobalMeasCounter_, &globalMeasCounter);
                    spdlog::debug("Global measure counter: {}", globalMeasCounter);
                    globalMeasCounter++;
                    if(!status)
                        status = setIntegerParam(GlobalMeasCounter_, globalMeasCounter);

                    for(int i = 0; i < numSelectedChannels; i++)
                    {
                        if(measChEna_[i])
                        {
                            sscanf(token, "%d,%lf,%d,%d,%lf,%lf,%lf,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", &channelNumber, 
                                                                                                    measuredLength + i,
                                                                                                    minimumIntensityRaw + i,
                                                                                                    maximumIntensityRaw + i,
                                                                                                    temperature + i,
                                                                                                    airPressure + i,
                                                                                                    humidity + i,
                                                                                                    analysisError + i,
                                                                                                    beamBreak + i,
                                                                                                    temperatureError + i,
                                                                                                    motionToleranceError + i,
                                                                                                    intensityError + i,
                                                                                                    connectionErrorUSB + i,
                                                                                                    tcpServerError + i,
                                                                                                    errorSettingLaserSpeed + i,
                                                                                                    laserTemperatureError + i,
                                                                                                    daqErrorDuringDataAcq + i);
                            token = strtok(NULL, "_");
                            errorResult[i] = ((analysisError[i] << ANALYSIS_ERROR_BIT_POSITION) +
                                                   (beamBreak[i] << BEAM_BREAK_BIT_POSITION) +
                                                   (temperatureError[i] << TEMPERATURE_ERROR_BIT_POSITION) +
                                                   (motionToleranceError[i] << MOTION_TOLERANCE_ERROR_BIT_POSITION) +
                                                   (intensityError[i] << INTENSITY_ERROR_BIT_POSITION) +
                                                   (connectionErrorUSB[i] << CONNECTION_ERROR_USB_BIT_POSITION) +
                                                   (tcpServerError[i] << TCP_SERVER_ERROR_BIT_POSITION) +
                                                   (errorSettingLaserSpeed[i] << ERROR_SETTING_LASER_SPEED_BIT_POSITION) +
                                                   (laserTemperatureError[i] << LASER_TEMPERATURE_ERROR_BIT_POSITION) +
                                                   (daqErrorDuringDataAcq[i] << DAQ_ERROR_DURING_DATA_ACQ_BIT_POSITION) + 
                                                   (tcpServerErrorFinal << TCP_SERVER_ERROR_FINAL_BIT_POSITION)) & errorMask_[i];
                            spdlog::debug("Channel: {} Error mask: 0x{:X} Error result: 0x{:X}", channelList_[i], errorMask_[i], errorResult[i]);
                            // Update last valid measurement if no errors
                            if(errorResult[i] == 0)
                            {
                                if(!status)
                                    status = doCallbacksFloat64Array(measuredLength, MAX_NUM_OF_CHANNELS, LastValidMeasuredLength_, 0);
                                if(!status)
                                    status = doCallbacksInt32Array(minimumIntensityRaw, MAX_NUM_OF_CHANNELS, LastValidMeasuredImin_, 0);
                                if(!status)
                                    status = doCallbacksInt32Array(maximumIntensityRaw, MAX_NUM_OF_CHANNELS, LastValidMeasuredImax_, 0);
                                if(!status)
                                    status = doCallbacksFloat64Array(temperature, MAX_NUM_OF_CHANNELS, LastValidMeasuredTemp_, 0);
                                if(!status)
                                    status = doCallbacksFloat64Array(airPressure, MAX_NUM_OF_CHANNELS, LastValidMeasuredPress_, 0);
                                if(!status)
                                    status = doCallbacksFloat64Array(humidity, MAX_NUM_OF_CHANNELS, LastValidMeasuredHum_, 0);
                                // Update valid measurement counter per channel
                                lastValidMeasCounter_[i] = globalMeasCounter;
                            }
                            measCounter_[i] = globalMeasCounter;
                        }
                    }
                    status = doCallbacksInt32Array(lastValidMeasCounter_, MAX_NUM_OF_CHANNELS, LastValidMeasCounter_, 0);
                    status = doCallbacksInt32Array(measCounter_, MAX_NUM_OF_CHANNELS, MeasCounter_, 0);
                    sscanf(token, "%d", &tcpServerErrorFinal);
                    if(!status)
                        status = setIntegerParam(TcpFinalServerError_, tcpServerErrorFinal);
                    if(!status)
                        status = callParamCallbacks();
                }
                // Update current measurement 
                if(!status)
                    status = doCallbacksFloat64Array(measuredLength, MAX_NUM_OF_CHANNELS, MeasuredLength_, 0);
                if(!status)
                    status = doCallbacksInt32Array(minimumIntensityRaw, MAX_NUM_OF_CHANNELS, MeasuredImin_, 0);
                if(!status)
                    status = doCallbacksInt32Array(maximumIntensityRaw, MAX_NUM_OF_CHANNELS, MeasuredImax_, 0);
                if(!status)
                    status = doCallbacksFloat64Array(temperature, MAX_NUM_OF_CHANNELS, MeasuredTemp_, 0);
                if(!status)
                    status = doCallbacksFloat64Array(airPressure, MAX_NUM_OF_CHANNELS, MeasuredPress_, 0);
                if(!status)
                    status = doCallbacksFloat64Array(humidity, MAX_NUM_OF_CHANNELS, MeasuredHum_, 0);
                if(!status)
                    status = doCallbacksInt32Array(analysisError, MAX_NUM_OF_CHANNELS, MeasuredAnalysisError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(beamBreak, MAX_NUM_OF_CHANNELS, MeasuredBeamInterruption_, 0);
                if(!status)
                    status = doCallbacksInt32Array(temperatureError, MAX_NUM_OF_CHANNELS, MeasuredTemperatureError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(motionToleranceError, MAX_NUM_OF_CHANNELS, MeasuredMovementToleranceError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(intensityError, MAX_NUM_OF_CHANNELS, MeasuredIntensityError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(connectionErrorUSB, MAX_NUM_OF_CHANNELS, MeasuredUsbConnectionError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(tcpServerError, MAX_NUM_OF_CHANNELS, MeasuredTcpServerError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(errorSettingLaserSpeed, MAX_NUM_OF_CHANNELS, MeasuredLaserSpeedError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(laserTemperatureError, MAX_NUM_OF_CHANNELS, MeasuredLaserTemperatureError_, 0);
                if(!status)
                    status = doCallbacksInt32Array(daqErrorDuringDataAcq, MAX_NUM_OF_CHANNELS, MeasuredDaqError_, 0);

                if(!status)
                    status = setIntegerParam(MeasState_, StateIdle);
            }
        }
    }
    else
    {
        spdlog::warn("There are no enabled channels, I am not going to send a measure request to the server. Setting status to ERROR.");
        if(!status)
            status = setIntegerParam(MeasState_, StateError);
    }
    return status;
}

asynStatus EtalonMultiline2::getAlignment(asynStatus status)
{
    char query[MAX_QUERY_SIZE];
    char reply[MAX_REPLY_SIZE];

    if(!status)
    {
        strcpy(query, "alignmentdata");

        char strChannel[4];

        for(int i = 0; i < numSelectedChannels; i++)
        {
            sprintf(strChannel, ",%d", channelList_[i]);
            strcat(query, strChannel);
        }
    }

    status = serverQuery(query, reply, status);

    double AlignDataMax[MAX_NUM_OF_CHANNELS];
    double AlignDataMin[MAX_NUM_OF_CHANNELS]; 
                
    if(!status)
    {
        char * token;

        token = strtok(reply, "_");
        token = strtok(NULL, "_");

        int index = 0;

        epicsInt32 channel;

        while(token != NULL)
        {
            sscanf(token, "%d,%lf,%lf", &channel, AlignDataMax + index, AlignDataMin + index);
            token = strtok(NULL, "_");
            index++;
        }
    }
    
    if(!status)
        status = doCallbacksFloat64Array(AlignDataMax, MAX_NUM_OF_CHANNELS, AlignDataMax_, 0);
    if(!status)
        status = doCallbacksFloat64Array(AlignDataMin, MAX_NUM_OF_CHANNELS, AlignDataMin_, 0);

    return status;
}

asynStatus EtalonMultiline2::writeInt32(asynUser *pasynUser, 
                                        epicsInt32 value)
{
    asynStatus status = asynSuccess;
    int function = pasynUser->reason;
    char reply[1000];
    char tmpQuery[1000];
    if(function == FESOption_)
    {
        sprintf(tmpQuery, "setfesoption,%d", value);
        status = serverQuery(tmpQuery, reply);

        if(strcmp(reply, "OK") != 0)
        {
            spdlog::error("Got no answer from device.");
        }
    }
    else if(function == RedPilotLaser_)
    {
        if(value == 1)
            status = serverQuery("piloton", reply);
        else if(value == 0)
            status = serverQuery("pilotoff", reply);
        else
            spdlog::error("Red pilot laser must be 1 (ON) or 0 (OFF) only.");
        
        if(strcmp(reply, "OK") != 0)
        {
            spdlog::error("No aknowlegde received from device's side.");
        }
    }
    else if(function == FrontEndSplitter_)
    {
        if(value)
            status = serverQuery("feson", reply);
        else
            status = serverQuery("fesoff", reply);
        
        if(strcmp(reply, "OK") != 0)
            spdlog::error("No aknowlegde received from device's side.");
    }
    else if(function == SingleMeasurement_)
    {
        if(value)
        {
            if(!status)
                status = setIntegerParam(MeasState_, StateRequested);
            
            epicsEventStatus statusEvent;
            statusEvent = epicsEventTrigger(startMeasEvent_);
            if(statusEvent != epicsEventOK)
                spdlog::error("Error while trying to trigger measurement event: statusEvent={}", statusEvent);
        }
    }
    else if(function == PollingPeriod_)
    {
        if(!status)
            status = setIntegerParam(PollingPeriod_, value);
    }
    else if(function == PollingEnable_)
    {
        if(value)
            pollingEnable_ = 1;
        else
            pollingEnable_ = 0;
    }
    else if(function == AlignmentStart_)
    {
        if(value)
        {
            epicsEventStatus statusEvent;
            statusEvent = epicsEventTrigger(startAlignEvent_);
            if(statusEvent != epicsEventOK)
                spdlog::error("Error while trying to trigger alignment event: statusEvent={}", statusEvent);
        }
        else
            if(!status)
                status = setIntegerParam(AlignmentStart_, 0);
    }
    else
        status = asynPortDriver::writeInt32(pasynUser, value);

    if(!status)
        status = callParamCallbacks();

    return status;
}

asynStatus EtalonMultiline2::readInt32Array(asynUser *pasynUser,
                                            epicsInt32 *value,
                                            size_t nElements,
                                            size_t *nIn)
{
    asynStatus status = asynSuccess;
    int function = pasynUser->reason;
    if(function == MeasEnableChannel_)
    {
        memcpy(measChEna_, value, MAX_NUM_OF_CHANNELS*sizeof(int));
    }
    else if(function == ErrorMask_)
    {
        memcpy(errorMask_, value, MAX_NUM_OF_CHANNELS*sizeof(int));
    }
    else
        status = asynPortDriver::readInt32Array(pasynUser, value, nElements, nIn);
    *nIn = nElements;
    return status;
}

asynStatus EtalonMultiline2::readFloat64Array(asynUser *pasynUser,
                                              epicsFloat64 *value,
                                              size_t nElements,
                                              size_t *nIn)
{
    asynStatus status = asynSuccess;
    int function = pasynUser->reason;
    if(function == MeasChannelPreshot_)
    {
        memcpy(measPreshot_, value, MAX_NUM_OF_CHANNELS*sizeof(epicsFloat64));
    }
    *nIn = nElements;
    return status;
}

extern "C" {
    int etalonMultiLine2Config(const char *portName, const char *octetPortName, bool enableDebug) {
        etalonPtr = new EtalonMultiline2(portName, octetPortName, enableDebug);
        return asynSuccess;
    }

    static const iocshArg configArg0 = { "Port name",       iocshArgString};
    static const iocshArg configArg1 = { "Octet port name", iocshArgString};
    static const iocshArg configArg2 = { "Debug",           iocshArgInt   };

    static const iocshArg * const configArgs[] = {&configArg0,
                                                  &configArg1,
                                                  &configArg2};

    static const iocshFuncDef configFuncDef = {"etalonMultiLine2Config", 3, configArgs};

    static void configCallFunc(const iocshArgBuf *args) {
        etalonMultiLine2Config(args[0].sval, args[1].sval, args[2].ival);
    }

    static void etalonMultiLine2Register(void) {
        iocshRegister(&configFuncDef, configCallFunc);
    }

    epicsExportRegistrar(etalonMultiLine2Register);
}
