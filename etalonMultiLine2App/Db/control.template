record(bo, "$(P)$(R)RedPilotLaser-S") {
    field(DESC, "Set Red Pilot Laser ON/OFF")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(ZNAM, "Pilot OFF")
    field(ONAM, "Pilot ON")
    field(OUT,  "@asyn($(PORT),$(ADDR))RED_PILOT_LASER")
}

record(bo, "$(P)$(R)FrontEndSplitter-S") {
    field(DESC, "Set FES ON/OFF")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(ZNAM, "FES OFF / Shutter ON")
    field(ONAM, "FES ON  / Shutter OFF")
    field(OUT,  "@asyn($(PORT),$(ADDR))FRONT_END_SPLITTER")
}

record(bo, "$(P)$(R)MeasStart-S") {
    field(DESC, "Start a single measurement")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(HIGH, "0.1")
    field(ZNAM, "DONE")
    field(ONAM, "RUNNING")
    field(OUT,  "@asyn($(PORT),$(ADDR))SINGLE_MEASUREMENT")
}

record(waveform, "$(P)$(R)MeasEnableChannel-S") {
    field(DESC, "Enable measurement for channel i")
    field(DTYP, "asynInt32ArrayIn")
    field(PINI, "YES")
    field(FTVL, "LONG")
    field(NELM, "40")
    field(INP,  "@asyn($(PORT),$(ADDR))MEAS_ENABLE_CHANNEL")
    info(autosaveFields, "VAL")
}

record(waveform, "$(P)$(R)MeasPreshot-S") {
    field(DESC, "Length preshot for channel i")
    field(DTYP, "asynFloat64ArrayIn")
    field(PINI, "YES")
    field(FTVL, "DOUBLE")
    field(NELM, "40")
    field(INP,  "@asyn($(PORT),$(ADDR))MEAS_CHANNEL_PRESHOT")
    info(autosaveFields, "VAL")
}

record(bo, "$(P)$(R)FESOption-S") {
    field(DESC, "Front end splitter option")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(OUT,  "@asyn($(PORT),$(ADDR))FES_OPTION")
    field(ZNAM, "FES option OFF")
    field(ONAM, "FES option ON")
}

record(ao, "$(P)$(R)PollingPeriod-S") {
    field(DESC, "Polling period in seconds")
    field(EGU, "s")
    field(DTYP, "asynInt32")
    field(PINI, "YES")
    field(VAL,  "0")
    field(OUT,  "@asyn($(PORT),$(ADDR))POLLING_PERIOD")
}

record(bo, "$(P)$(R)PollingEnable-S") {
    field(DESC, "Polling period in seconds")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(ZNAM, "False")
    field(ONAM, "True")
    field(OUT,  "@asyn($(PORT),$(ADDR))POLLING_ENABLE")
}

record(bo, "$(P)$(R)AlignmentStart-S") {
    field(DESC, "Start/Stop alignment of channels")
    field(DTYP, "asynInt32")
    field(VAL,  "0")
    field(ZNAM, "False")
    field(ONAM, "True")
    field(OUT,  "@asyn($(PORT),$(ADDR))ALIGNMENT_START")
}

record(waveform, "$(P)$(R)ErrorMask-S") {
    field(DESC, "Mask to enable checking errors")
    field(DTYP, "asynInt32ArrayIn")
    field(PINI, "YES")
    field(FTVL, "LONG")
    field(INP,  "@asyn($(PORT),$(ADDR))ERROR_MASK")
    field(NELM, "40")
    info(autosaveFields, "VAL")
}

record(ao, "$(P)$(R)GlobalMeasCounter-S") {
    field(DESC, "Set measurement counter")
    field(DOL, "$(P)$(R)GlobalMeasCounter-R")
    field(OMSL, "closed_loop")
    field(DTYP, "asynInt32")
    field(PINI, "YES")
    field(OUT,  "@asyn($(PORT),$(ADDR))GLOBAL_MEAS_COUNTER")
}
