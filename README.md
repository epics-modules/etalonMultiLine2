# etalonMultiLine2

European Spallation Source ERIC Site-specific EPICS module: etalonMultiLine2

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/ESTIA+-+Etalon+Multiline+Absolute+Distance+Interferometer)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/etalonMultiLine2-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
