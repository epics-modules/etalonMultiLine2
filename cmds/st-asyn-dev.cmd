require etalonMultiLine2
require autosave

# autosave config
epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "etalon")
epicsEnvSet("IOCDIR", "$(IOCNAME)")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(TOP), IOCNAME=$(IOCNAME), IOCDIR=$(IOCDIR)")

epicsEnvSet(IPADDR,     "localhost")
epicsEnvSet(IPPORT,     "8080")
epicsEnvSet(PREFIX,     "ESTIA-Sel1:Mech-GU-001")
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(SLOW_SCAN,  "1")
epicsEnvSet(FAST_SCAN,  "1")

epicsEnvSet(STREAM_PROTOCOL_PATH, "$(etalonMultiLine2_DIR)db")

drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

#asynSetTraceMask("$(PORTNAME)",-1,0x9) 
#asynSetTraceIOMask("$(PORTNAME)",-1,0x2)

etalonMultiLine2Config("ETALON", "$(PORTNAME)", 1)

dbLoadRecords(control.db, "P=$(PREFIX),R=:, PORT="ETALON", ADDR=0, SLOW_SCAN=$(SLOW_SCAN), FAST_SCAN=$(FAST_SCAN)")
dbLoadRecords(data.db, "P=$(PREFIX),R=:, PORT="ETALON", ADDR=0, SLOW_SCAN=$(SLOW_SCAN), FAST_SCAN=$(FAST_SCAN)")
dbLoadRecords(error.db, "P=$(PREFIX),R=:, PORT="ETALON", ADDR=0, SLOW_SCAN=$(SLOW_SCAN), FAST_SCAN=$(FAST_SCAN)")
dbLoadRecords(status.db, "P=$(PREFIX),R=:, PORT="ETALON", ADDR=0, SLOW_SCAN=$(SLOW_SCAN), FAST_SCAN=$(FAST_SCAN)")

iocInit()
