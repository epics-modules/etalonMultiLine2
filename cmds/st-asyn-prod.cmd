require etalonMultiLine2
require autosave

# autosave config
epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "etalon")
epicsEnvSet("IOCDIR", "$(IOCNAME)")

# etalon config
epicsEnvSet("P", "ESTIA-Sel1")
epicsEnvSet("R", "Mech-GU-001")
epicsEnvSet(IPADDR, "estia-sel1-intfer-01.cn.nin.ess.eu")
epicsEnvSet(IPPORT, "2001")

iocshLoad("$(etalonMultiLine2_DIR)etalonMultiLine2.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
iocshLoad("$(autosave_DIR)autosave.iocsh", "AS_TOP=$(TOP), IOCNAME=$(IOCNAME), IOCDIR=$(IOCDIR)")

iocInit()
